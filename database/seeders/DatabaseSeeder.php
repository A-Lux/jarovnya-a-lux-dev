<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(
            [
                'title' => 'На доставке'
            ]
        );
        DB::table('statuses')->insert(
            [
                'title' => 'Выполнен'
            ]
        );

        DB::table('order_types')->insert(
            [
                'title' => 'Доставка'
            ]
        );

        DB::table('order_types')->insert(
            [
                'title' => 'Предзаказ'
            ]
        );

        DB::table('order_types')->insert(
            [
                'title' => 'Самовызов'
            ]
        );


        DB::table('categories')->insert(
            [
                'title' => 'Блюда',
                'image' => 'sd'
            ]
        );
        DB::table('categories')->insert(
            [
                'title' => 'Кондитерская',
                'image' => 'sd'
            ]
        );
        DB::table('categories')->insert(
            [
                'title' => 'Бар',
                'image' => 'sd'
            ]
        );



        \App\Models\Product::factory(10)->create();
    }
}
