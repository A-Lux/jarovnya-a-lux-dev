<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->float('price');
            $table->text('description');
            $table->string('image');
            $table->text('with_products')->nullable();
            $table->text('keywords')->nullable();
            $table->tinyInteger('is_new')->default(0);
            $table->tinyInteger('is_day')->default(0);
            $table->float('discount_price')->nullable();
            $table->unsignedBigInteger('order')->default(1);
            $table->foreignId('category_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
