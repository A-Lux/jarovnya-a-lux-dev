<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function scopeNew($query)
    {
        return $query->where('is_new', 1);
    }

    public function scopeDay($query)
    {
        return $query->where('is_day', 1);
    }

    public function scopeDiscount($query)
    {
        return $query->where('discount_price', '<>', null);
    }

}
