<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function newProducts()
    {
        return $this->hasMany(Product::class)->where('is_new', 1);
    }

    public function dayProducts()
    {
        return $this->hasMany(Product::class)->where('is_day', 1);
    }

    public function discountProducts()
    {
        return $this->hasMany(Product::class)->where('discount_price', '<>', null);
    }
}
