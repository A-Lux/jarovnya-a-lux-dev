<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\WishList;
use Illuminate\Http\Request;
use Validator;

class WishListController extends Controller
{
    public function show(Request $request)
    {
        $list = $request->user()->wishes;
        return response()->json(['list' => $list], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Выберите продукт'], 400);
        }
        $wishList = new WishList();
        $wishList->user_id = $request->user()->id;
        $wishList->product_id = $request->product_id;
        $wishList->save();
        return response()->json(['message' => 'Ok'], 200);
    }

    public function drop(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Выберите продукт'], 400);
        }
        $wishList = WishList::where(['product_id' => $request->product_id, 'user_id' => $request->user()->id])->first();
        if ($wishList)
            $wishList->delete();
        return response()->json(['message' => 'Ok'], 200);
    }
}
