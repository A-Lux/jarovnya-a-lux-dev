<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function show(Request $request)
    {
        return response()->json(['user' => $request->user()], 200);
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate(
            [
                'name' => 'string|max:255',
                'email' => 'email|max:255|unique:users,email,' . $request->user()->id,
                'phone' => 'string|min:11|max:11|unique:users,phone,' . $request->user()->id,
                'password' => 'nullable|string|min:6|max:255|confirmed',
                'birthday' => 'nullable|date'
            ]
        );
        $validatedData['password'] = Hash::make($request->password);

        $request->user()->update($validatedData);
        return response()->noContent();
    }

}
