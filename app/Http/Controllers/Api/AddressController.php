<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function show(Request $request)
    {
        $addresses = $request->user()->addresses;
        return response()->json(['addresses' => $addresses], 200);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'title' => 'string|max:255'
            ]
        );
        $validatedData['user_id'] = $request->user()->id;
        Address::create($validatedData);
    }

}
