<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = Category::orderBy('order')->with('products', function ($query) {
            $query->orderBy('order');
        })->get();

        return response()->json(['Products' => $products], 200);
    }

    public function allNew()
    {
        $products = Product::new()->orderBy('order')->get();
        return response()->json(['Products' => $products], 200);
    }

    public function categoryNew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Выберите категорию'], 400);
        }

        if ($category = Category::find($request->category_id)) {
            $products = $category->newProducts;
            return response()->json(['Products' => $products], 200);
        }

        return response()->json(['message' => 'Категории не существует'], 404);

    }

    public function allDay()
    {
        $products = Product::day()->orderBy('order')->get();
        return response()->json(['Products' => $products], 200);
    }

    public function categoryDay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Выберите категорию'], 400);
        }
        if ($category = Category::find($request->category_id)) {
            $products = $category->dayProducts;
            return response()->json(['Products' => $products], 200);
        }
        return response()->json(['message' => 'Категории не существует'], 404);
    }


    public function allDiscount()
    {
        $products = Product::discount()->orderBy('order')->get();
        return response()->json(['Products' => $products], 200);
    }

    public function categoryDiscount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Выберите категорию'], 400);
        }
        if ($category = Category::find($request->category_id)) {
            $products = $category->dayProducts;
            return response()->json(['Products' => $products], 200);
        }
        return response()->json(['message' => 'Категории не существует'], 404);
    }


}
