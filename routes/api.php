<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', 'AuthController@login')->name('login');
Route::post('/register', 'AuthController@register')->name('register');

Route::get('/products', 'ProductController@index');

Route::prefix('/product')->name('product.')->group(function () {
    Route::get('/new', 'ProductController@allNew');
    Route::get('/category/new', 'ProductController@categoryNew');

    Route::get('/day', 'ProductController@allDay');
    Route::get('/category/day', 'ProductController@categoryDay');

    Route::get('/discount', 'ProductController@allDiscount');
    Route::get('/category/discount', 'ProductController@categoryDiscount');

});

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', 'AuthController@logout')->name('logout');


    Route::prefix('/user')->name('user.')->group(function () {
        Route::get('/show', 'UserController@show');
        Route::put('/update', 'UserController@update');
    });

    Route::prefix('/address')->name('address.')->group(function () {
        Route::get('/show', 'AddressController@show');
        Route::post('/create', 'AddressController@store');
    });

    Route::prefix('/wish-list')->group(function () {
        Route::get('/show', 'WishListController@show');
        Route::post('/create', 'WishListController@store');
        Route::delete('/delete', 'WishListController@drop');
    });

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
});

